#This script will grab most recent tracks from:
#https://www.radio4all.net/files/ruralwarroom@yahoo.com/?C=M;O=D
#and put them in the Rural War Room folder for Megaseg, renamed to hour1.MP3, hour2.MP3, etc...
#In addition, it will back up last week's show to rwr/backup folder after deleting previous backup.
 
import requests, os, re, requests, shutil, glob
from datetime import datetime, time, date
from bs4 import BeautifulSoup
from operator import itemgetter
from urllib.request import urlretrieve

siteURL="http://www.radio4all.net/files/ruralwarroom@yahoo.com/"
organizedSiteURL="http://www.radio4all.net/files/ruralwarroom@yahoo.com/?C=M;O=D"
links=[]
arrangedLinks=[]

class LinkInfo:

    def __init__(self):
        self.newestDate=datetime(2000,1,1,1,1,1)
    
    #Gets links, dates, and times from site for first 8 mp3 files.
    def getLinkInfo(self):
        cleanLinks=[]
        choppedLinks=[]
        #Get site.
        r=requests.get(organizedSiteURL)
        #Make site beautiful:
        soup=BeautifulSoup(r.text, "html.parser")
        #Get 'td' tags from site.
        tdsoup=soup.find_all('td')
        for child in tdsoup:
            if child.a in child:
                dirtyLink=(child.a.get('href'))
                if (dirtyLink.lower()).endswith('.mp3'):
                    siblingText=child.next_sibling.text
                    dateMatch=re.search("\d{4}-\d{2}-\d{2} \d{2}:\d{2}", siblingText)
                    trackDate=datetime.strptime(str(dateMatch.group(0)), '%Y-%m-%d %H:%M')
                    cleanLinks.append([dirtyLink, trackDate])
            if len(cleanLinks)==8:
                break
        sortedLinks=self.sortLinks(cleanLinks, self.newestDate)
        for link in sortedLinks:
            print (link)
        return sortedLinks

    #Remove links to tracks that don't match newest date, then sort by time.
    def sortLinks(self, cleanLinks, newestDate):
        for i, link in enumerate(cleanLinks):
            if datetime.date(link[1]) < datetime.date(newestDate):
                cleanLinks.remove(link)
                self.sortLinks(cleanLinks, newestDate)
            elif datetime.date(link[1]) > datetime.date(newestDate):
                newestDate=link[1]
                self.sortLinks(cleanLinks, newestDate)
        sortedLinks=sorted(cleanLinks, key=lambda x: datetime.time(x[1]), reverse=True)
        return sortedLinks

#Set up trackTitleOnDrive1-4.mp3 naming structure for external drive files and actually get tracks.
def implementFileExchange(links):
    trackTitleOnDrive=[]
    #Change working directory to RWR:
    os.chdir('/Volumes/KUHS Music/DJ Audio Files/Rural War Room')
    #Sort links by time.
    configureBackups()
    for i in range(0,len(links)):
        #Create file name template.
        trackTitleOnDrive.append("hour"+str(i+1)+".MP3")
        #Set external path for writing url to trackTitleOnDrive file.
        externalFileName=str(os.path.join('./'+str(trackTitleOnDrive[i])))
        #Create link to mp3 file at radio4all:
        track=str(links[i][0])
        trackURI=(siteURL+track)
        #Download track to external as trackTitleOnDrive1-4.mp3.
        urlretrieve(trackURI, externalFileName)

#Make backup copy of last week's show in backup folder. Because backups are good.
def configureBackups():
    #Delete previous backup of last week show if it exists.
    oldBackup=glob.glob(os.path.join('./backup/'+'*.MP3'))
    for file in oldBackup:
        if os.path.exists(file):
            print ('Removing ',file,' from backup folder...')
            os.remove(file)
    #Move last week's show to backup folder if it exists.
    currentBackup=glob.glob(os.path.join('./'+'*.MP3'))
    for file in currentBackup:
        if os.path.exists(file):
            print ('Putting ',file,' in backup folder.')
            destination=('./backup/'+str(file))
            shutil.move(file, destination)

LI = LinkInfo()
linkInfo=LI.getLinkInfo()
implementFileExchange(linkInfo)
